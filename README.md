Subtree/submodule workflow

    0. PoC setup. Procedure
        - First thing for this example is to create a folder with some simple HTML and CSS files to play with
        - Next step is to make some changes, commits, branches, more commits, merges, etc...
        - Be sure that the code you want to split is in its own folder
        - Push the repo into Bitbucket
        - Then it should be ready (changes in this file should not be taken into consideration for its git story)
        - Keep in mind that there are different approaches depending on the situation. One can either add a subrepo or split part of a main repo to its own

    1. Split folder (library/service/etc) from the current monolithic repository to its own git project  (this will create a bunch of files and folders in your module folder)
        - First, start at root folder of main repo
            $ cd <module_folder>
        - then turn folder into a new bare repository
	        $ git init --bare
        - go to root folder and add the module as a subtree
            $ cd ..
            $ git subtree split --prefix=<module_folder>
        - create a remote repository and add it to access it easily
            $ git remote add <remote_name> <remote_repository>
        - now you can push commits
            $ git subtree push --prefix=<module_folder> <remote_name> <ref>
        


    2. Put the split code in its own repository (push and yada yada)
    3. Add to the dependent repositories as needed (subtree or submodule)
    4. Perform pulls and pushes as it goes

Note:

    * In case of git commands turning to long or complicaded, maybe possible to run them as (git/node) scripts?
        - Problem: subtree wont update autmaticatelly to its own remote repository when git push is runned, but git subtree push is requiered. This can besolved with git hooks

    * Problem: when cloning the super repository, all files are cloned - subtree included -, but the repository don't "remember", that part of it is a subrepository\
    


References
    https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt


